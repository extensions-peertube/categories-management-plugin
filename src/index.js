const { toEnhancedCategories } = require('./enhancedCategories');
const {
  handleChanges,
  storeInitialCategories,
  getCustomCategories,
  customCategoriesInitialState,
} = require('./server/settings');
const { renderEnhancedCategoriesTable } = require('./server/renderers');
const customCategoriesExample = require('../data/categories.json');

async function register({
  registerSetting,
  registerHook,
  settingsManager,
  storageManager,
  videoCategoryManager,
  peertubeHelpers,
  getRouter,
}) {
  const { logger, config } = peertubeHelpers;

  registerHook({
    target: 'action:application.listening',
    handler: async () => {
      await storeInitialCategories({ config, storageManager, logger });

      await handleChanges({
        videoCategoryManager,
        storageManager,
        config,
        logger,
      })(await settingsManager.getSettings(['json-categories-as-text']));
    },
  });

  registerSetting({
    name: 'json-categories-as-text',
    label: 'Categories to add/delete',
    type: 'input-textarea',
    default: JSON.stringify(customCategoriesInitialState),
    descriptionHTML: `Describe in Json the categories to <i>add/delete</i> respecting the following format:<br />
<pre><code>${JSON.stringify(customCategoriesExample)}</pre></code>`,
  });

  try {
    // fetch instance original categories from the PeerTube API
    await storeInitialCategories({
      config,
      storageManager,
      logger,
    });
  } catch (e) {
    logger.debug('if this fails at instance startup this is kind of normal');
  }
  const categoriesEndpoint = `${config.getWebserverUrl()}/api/v1/videos/categories`;
  registerSetting({
    name: 'categoriesTable',
    label: 'Preview',
    type: 'html',
    html: renderEnhancedCategoriesTable([]),
    descriptionHTML: `You may verify which categories will actually be exposed there: <a href="${categoriesEndpoint}" target="_blank">${categoriesEndpoint}</a>`,
  });

  // @todo in case of an install and if there were previous setting, apply them
  // otherwise the user need to hit submit setting again

  settingsManager.onSettingsChange(
    handleChanges({
      videoCategoryManager,
      storageManager,
      config,
      logger,
    })
  );

  const router = getRouter();
  router.get('/enhanced-categories', async (req, res) => {
    try {
      const { delete: deletedCategories, add: addedCategories } =
        await getCustomCategories({ settingsManager });
      const categories = await storageManager.getData('initialCategories');

      logger.debug('initial categories', categories);
      logger.debug('deleted categories', deletedCategories);
      logger.debug('added categories', addedCategories);

      const enhancedCategories = toEnhancedCategories({
        categories,
        deletedCategories,
        addedCategories,
      });
      if (req.accepts('application/json')) {
        return res.json(enhancedCategories);
      }

      return res.send(renderEnhancedCategoriesTable(enhancedCategories));
    } catch (e) {
      logger.log({ level: 'error', message: e });
      if (req.accepts('application/json')) {
        return res
          .status(500)
          .set('Content-Type', 'application/problem+json')
          .json({
            type: '',
            title: 'An internal server error occurred',
            status: 500,
          });
      }

      res.sendStatus(500);
    }
  });
}

async function unregister() {}

module.exports = {
  register,
  unregister,
};
