# Categories management plugin

A __PeerTube__ plugin to manage video categories.

## Development

1. Install and start a __PeerTube__ *dev* instance following this guide:
https://docs.joinpeertube.org/contribute-plugins?id=test-your-plugintheme

> *In dev mode, administrator username is __root__ and password is __test__.*

2. Install the plugin from your __PeerTube__ directory: 
```
node ./dist/server/tools/peertube.js plugins install --path ~/workspace/peertube/peertube-plugin-categories
```

You may look at the plugin categories configuration there:
http://localhost:9000/plugins/categories/router/enhanced-categories
> This URL is used to update the categories table on the settings page.

You may check which categories will actually be exposed in your instance:
http://localhost:9000/api/v1/videos/categories

## Deployment

### Force plugin update
1. set the latest available plugin version to the last version number:
```
update "plugin" set "latestVersion" = 'X.X.X' where "plugin"."name" = 'categories';
```
2. Refresh plugins page and *update* the plugin.

### Common pitfalls

- You must re-install the plugin to test each updates.

- If your client code doesn't seem to update, visit the following URL and clear your browser cache by hitting `Ctrl+F5`.
http://localhost:9000/plugins/categories/1.1.0/client-scripts/src/client/admin-plugin-settings.js
  
## Resources

- https://docs.joinpeertube.org/contribute-plugins
- https://framagit.org/framasoft/peertube/official-plugins/-/blob/master/peertube-plugin-hello-world/main.js

## License

[EUPL](LICENSE)
